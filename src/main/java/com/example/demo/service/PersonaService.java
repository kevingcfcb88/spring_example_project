package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Persona;

@Service
public class PersonaService {
	
	private static List<Persona> listPersonas=new ArrayList<Persona>();
	
	static {
		listPersonas.add(new Persona("Linus","Torvalds",new Date()));
	}
	
	public List<Persona> getAllPersonas() {
		return listPersonas;
	}
	
}
