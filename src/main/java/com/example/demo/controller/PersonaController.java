package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.service.PersonaService;

@Controller
public class PersonaController {

	@Autowired
	private PersonaService service;
	
	@GetMapping("/personas")
	/*
	 * Esta es la otra notación, pero al parecer se requiere otra dependencia
	 * @RequestMapping("/personas", method=RequestMethod.GET)
	 * */
	public String showPersonas(ModelMap model) {
		model.put("personas",service.getAllPersonas());
		return "welcome";
	}
	
}
